local json = require 'dkjson'

-- Creates a dataset with all the file names and their labels from a list text file.

function CreateDataset(list)

  Log(" > Reading list dataset from " .. list .. ":\n")
  --Ids of the dataset.
  ids = {}
  file = io.open(list, "r");
	line = io.input(file);

	while true do
		line = io.read();
		if line == nil then
       break
    end
		table.insert(ids, line)
	end
	io.close(file)

  gtFile = './groundtruth.json'
  local f = io.open(gtFile, "r")
  local str = f:read("*all")
  f:close()
	local gt, pos, err = json.decode(str, 1, nil)

  local dataset = {}

  dataset.FileNames = {}
  dataset.Labels = {}
  dataset.numSamples = 0

  for i,id in pairs(ids) do
    table.insert(dataset.FileNames,id)
    table.insert(dataset.Labels,gt[id]['type'])
    dataset.numSamples = dataset.numSamples + 1
  end

return dataset
end


-- TODO: Create functions for normalize Dataset.
