--Tests an epoch on the dataset

function testEpoch(dataset, model)
  accuracy = 0
  positive = 0
  negative = 0
  for sample = 1,dataset.numSamples do
    local input = torch.Tensor(1,3,256,256)
    local im = loadImageRescaled(dataset.FileNames[sample],256,256)
    input[1] = im

    if dataset.Labels[sample] == 'bra' then
      target = 1
    elseif dataset.Labels[sample] == 'panties' then
      target = 2
    end
    output = model:forward(input)
    if (output[1] > output[2] and target == 1) or (output[1] < output[2] and target == 2) then
      positive = positive + 1
    else
      negative = negative + 1
    end
  end
  print("Accuracy = ".. (positive/(positive+negative)))
end
