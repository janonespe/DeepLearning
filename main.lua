
require 'optim'
require 'nn'
require 'image'

package.path = './functions/?.lua;' .. package.path

require 'dataset'
require 'log'
require 'model'
require 'maths'
require 'trainEpoch'
require 'testEpoch'
require 'images'
-- Load datasets
trainDataset = CreateDataset('./trainList.txt')
testDataset = CreateDataset('./testList.txt')

model = CreateModel()

-- Parameters:

nEpochs = 20
criterion = nn.ClassNLLCriterion()
-- Train loop:

for epoch=0,nEpochs-1 do
  Log("#####################\nStarting Epoch " .. epoch + 1 .. "\n#####################\n")
  trainEpoch(trainDataset,model,criterion)
  --torch.save('./models/model-'.. epoch  .. '.torch', model)
end
Log(" > Testing on test dataset")
testEpoch(testDataset,model)
torch.save('./model.torch', model)
