require 'image'

function loadImageRescaled(path,sizeX,sizeY)
	im = image.load(path, 3, float);
	return resizeImage(im,sizeX,sizeY)
end

function resizeImage(im, sizeX, sizeY)
	imRescaled = torch.DoubleTensor(im:size(1), sizeX, sizeY);
	image.scale(imRescaled, im);
	return imRescaled
end

function loadImage(path)
	im = image.load(path, 3, float);
	return im
end


--TODO: Normalize image on loading with dataset mean and std
