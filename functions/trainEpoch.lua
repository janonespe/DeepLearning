-- Performs one epoch of training:
require 'math'


function trainEpoch(dataset,model,criterion)
  local optimState = {learningRate=0.1,momentum = 0.9,weightDecay = 0.0005}
  batchSize = 16

  --trainingOrder = CreateRandomVector(dataset.numSamples)
  local shuffle = torch.randperm(dataset.numSamples)
  for t = 1,dataset.numSamples, batchSize do
    --print("t is " ..t.. ", numSamples is " .. dataset.numSamples .. ", the remaining is " .. dataset.numSamples-t+1)
    -- Create batch:
    batchSamples = math.min(batchSize,dataset.numSamples-t+1)
    if batchSamples == 0 then break end

    local inputs = torch.Tensor(batchSamples,3,224,224)
    local targets = torch.Tensor(batchSamples):fill(1)

    inputs,targets = prepareInputsAndTargets(inputs,targets,dataset,t,batchSamples,shuffle,batchSize)

    local params, gradParams = model:getParameters()
    local function feval(params)
      gradParams:zero()

      local outputs = model:forward(inputs)
      local loss = criterion:forward(outputs, targets)
      local dloss_doutput = criterion:backward(outputs, targets)
      model:backward(inputs, dloss_doutput)
      return loss,gradParams
    end
    loss,_ = optim.sgd(feval, params, optimState)

    print("Batch " .. ((t-1)/batchSize)+1 .. "/" .. math.floor(dataset.numSamples/batchSize) .. ": loss: " .. loss[1])
    collectgarbage()
  end
  testEpoch(dataset, model)
end

function prepareInputsAndTargets(inputs,targets,dataset,t,numSamples,shuffle,batchSize)
  local iSampleInBatch=1
  for iSample = t,math.min(t+batchSize-1,dataset.numSamples) do
    im = loadImageRescaled(dataset.FileNames[shuffle[iSample]],224,224)
    inputs[iSampleInBatch] = im
    if dataset.Labels[shuffle[iSample]] == 'bra' then
      targets[iSampleInBatch] = 1
    elseif dataset.Labels[shuffle[iSample]] == 'panties' then
      targets[iSampleInBatch] = 2
    end
    iSampleInBatch = iSampleInBatch+1
  end
return inputs,targets
end
