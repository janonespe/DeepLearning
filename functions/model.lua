-- Creates the model:

function CreateModel()

  network = nn.Sequential()
  network:add(nn.SpatialConvolution(3,16,11,11,4,4))
  network:add(nn.ReLU())
  network:add(nn.SpatialMaxPooling(2,2,2,2))

  network:add(nn.SpatialConvolution(16,32,3,3))
  network:add(nn.ReLU())
  network:add(nn.SpatialMaxPooling(2,2,2,2))
  network:add(nn.Dropout())

  network:add(nn.View(32*12*12))
  network:add(nn.Linear(32*12*12,1024))
  network:add(nn.Linear(1024,2))
  network:add(nn.LogSoftMax())

  return network
end
