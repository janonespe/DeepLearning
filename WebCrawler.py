#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import json
import numpy as np

def crawlObject(href,driver):
    global ontology,objectType,objectSubType,objectsDone,jsonOntology
    print "Crawling " + href
    previous_url = driver.current_url
    driver.get(href)
    colors = driver.find_elements_by_xpath("/html/body/section[@id='wrapper']/section[@id='content']/form[1]/section[@class='product']/ul[1]/li[2]/fieldset[1]/section[1]/div[1]/a")
    objectOntology = []

    print str(len(colors)) + " colors found."
    for idxcolor,color in enumerate(colors):
        #Change the view to each color
        try:
            color.click()
            print "Color changed."
        except Exception as e:
            #Close the add!
            print "Exception raised because of add. Closing add."
            addCloser = driver.find_elements_by_css_selector(".acs-CloseButton.acsAbandonButton")
            print len(addCloser)
            if len(addCloser) == 1:
                action = webdriver.common.action_chains.ActionChains(driver)
                action.move_to_element_with_offset(addCloser[0], 5, 5)
                action.click()
                action.perform()
                print "Add closed. Resuming."
        #Click on the object centered image to download:
        objectCenteredSelector = driver.find_elements_by_xpath("/html/body/section[@id='wrapper']/section[@id='content']/form[1]/section[@class='product']/div[1]/div[@id='alt-images']/ul[1]/li[@data-image-type='OffModelFront']")
        if len(objectCenteredSelector) == 0:
            print "Front model not found"
            continue
        print objectCenteredSelector[0].get_attribute("data-image-type")
        action = webdriver.common.action_chains.ActionChains(driver)
        action.move_to_element_with_offset(objectCenteredSelector[0], 5, 5)
        action.click()
        action.perform()
        #Download the image:
        imageURL = driver.find_element_by_xpath("/html/body/section[@id='wrapper']/section[@id='content']/form[1]/section[@class='product']/div[1]/div[@id='mainView']/img[@id='vsImage']").get_attribute("src")
        #Check for the image in the ontology:
        if imageURL in ontology.keys():
            print "Image already present on the ontology. Skipping."
            continue
        print "URL saved. [" + str(idxcolor + 1) + "/" + str(len(colors)) + "]"

        #call to system to download the image, as Victoria's Secret asks for a valid user-agent, and urllib does not work. This does the trick.
        os.system('wget --user-agent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/53" -q -P ./VictoriasSecret ' + imageURL )

        #append this image to the list of objects for this model
        subObjectOntology = {}
        subObjectOntology['url'] = imageURL[:-4]
        subObjectOntology['type'] = objectType
        subObjectOntology['subType'] = objectSubType
        subObjectOntology['extension'] = imageURL[-4:]
        objectOntology.append(subObjectOntology)
        objectsDone = objectsDone + 1
        if np.mod(objectsDone,50) == 0:
            print "##########\n" + str(objectsDone) + " objects crawled.\n##########"

    ontology[href] = objectOntology-
    with open(jsonOntology,'w') as f:
        json.dump(ontology,f)
    driver.get(previous_url)

def crawlCatalog(href,driver):
    global ontology,objectType,objectSubType,objectsDone,jsonOntology
    previous_url = driver.current_url
    driver.get(href)
    items = driver.find_elements_by_xpath("/html/body/section[@id='wrapper']/section[@id='content']/section[@id='collection-set']/ul/li/a[@href]")
    items = [it.get_attribute("href") for it in items]
    print str(len(items)) + " items found."
    for itIdx,it in enumerate(items):
        if it not in ontology.keys():
            crawlObject(it,driver)
        else:
            print it + " already crawled. Skipping."
        print str(itIdx+1) + "/" + str(len(items))
    driver.get(previous_url)


if __name__ == "__main__":

    #Open Victoria's Secret webpage on firefox
    driver = webdriver.Firefox()
    driver.get("http://www.victoriassecret.com")
    driver.maximize_window()

    #Force the closing of the sign in form letter. Sometimes it does not close automatically and the crawler crashes.
    print "Closing sign-in form..."
    addCloser = driver.find_elements_by_css_selector(".fab-ico--overlay-close.close")
    if len(addCloser) == 1:
        addCloser[0].click()

    print "Sign-in form closed"

    #global ontology,objectType,objectSubType,objectsDone,jsonOntology
    objectsDone = 0
    #Check for previous ontology:
    jsonOntology = './VictoriasSecretOntology.json'
    if os.path.isfile(jsonOntology):
        with open(jsonOntology) as f:
            ontology = json.load(f)
    else:
        ontology = {}

    # Different catalogs go here. #TODO: make this less hardcoded, or at least prettier. make the scrapper go at each style section.

    objectType = 'bra'

    '''objectSubType = 'bralette'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/bralette',driver)

    objectSubType = 'unlined'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/unlined',driver)

    objectSubType = 'lightly-lined'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/lightly-lined-and-demi',driver)

    objectSubType = 'full-coverage'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/full-coverage',driver)

    objectSubType = 'push-up'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/push-up',driver)

    objectSubType = 'lounge-and-wireless'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/lounge-and-wireless',driver)

    objectSubType = 'strapless-and-backless'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/bras/strapless-and-backless',driver)

    objectSubType = 'sports-bras'
    print objectSubType
    #crawlCatalog('https://www.victoriassecret.com/bras/sports-bras',driver)
'''
    objectType = 'panties'

    objectSubType = 'cheekies-and-cheekinis'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/cheekies-and-cheekinis',driver)


    objectSubType = 'thongs-and-v-strings'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/thongs-and-v-strings',driver)

    objectSubType = 'boyshorts-shorties'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/boyshorts-shorties',driver)

    objectSubType = 'hiphuggers'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/hiphuggers',driver)

    objectSubType = 'bikinis'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/bikinis',driver)

    objectSubType = 'no-show-and-seamless'
    print objectSubType
    crawlCatalog('https://www.victoriassecret.com/panties/no-show-and-seamless',driver)

    with open('./VictoriasSecretOntology.json','w') as f:
        json.dump(ontology,f)
