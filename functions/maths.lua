-- Creates a vector from 1 to length with all the values randomly sorted.

function CreateRandomVector(length)
  vector = torch.range(1, length)
  randomVector = torch.randperm(vector:size()[1])
  return randomVector
end
