#!/usr/bin/env python

import json

jsonOntology = './VictoriasSecretOntology.json'

with open(jsonOntology) as f:
    ontology = json.load(f)

newOntology = {}

panties = []
bras = []

#Move through the old item-colors ontology and extract each element as an individual item:
for key in ontology:
    if len(ontology[key]) > 0:
        for elem in ontology[key]:
            #Do not add repeated items.
            fileLocation = './VictoriasSecret/' + elem['url'].split('/')[-1] + elem['extension']
            if fileLocation in newOntology.keys():
                continue
            elemData = {}
            elemData['file'] = elem['url'].split('/')[-1]
            elemData['type'] = elem['type']
            elemData['subType'] = elem['subType']
            newOntology[fileLocation] = elemData

            if elem['type'] == 'bra':
                bras.append(fileLocation)
            elif elem['type'] == 'panties':
                panties.append(fileLocation)
with open('groundtruth.json','w') as f:
    json.dump(newOntology,f)


#Lists of panties and bras have been created
validationDatasetPercentage = 0.2

pantiesTrain = panties[:int((1-validationDatasetPercentage)*len(panties))]
pantiesTest = panties[int((1-validationDatasetPercentage)*len(panties)) + 1:]

braTrain = bras[:int((1-validationDatasetPercentage)*len(bras))]
braTest = bras[int((1-validationDatasetPercentage)*len(bras)) + 1:]

with open('trainList.txt','w') as f:
    for bra in braTrain:
        f.write("%s\n" % bra )
    for panties in pantiesTrain:
        f.write("%s\n" % panties)

with open('testList.txt','w') as f:
    for bra in braTest:
        f.write("%s\n" % bra )
    for panties in pantiesTest:
        f.write("%s\n" % panties)

print "Training list has " + str(len(braTrain) + len(pantiesTrain))
print "Testing list has " + str(len(braTest) + len(pantiesTest))
